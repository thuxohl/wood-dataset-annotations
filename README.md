# Wood Image Dataset - Annotations

This repository contains pixel-wise annotations for a dataset of spruce wood (Silvén et al. 2003).
The images in the dataset contain black borders with timestamps.
Before applying these pixel-wise annotations you have to cut of the black borders using the region labels provided in the original dataset. 

## Citation
If you use this dataset please cite our Paper:

`T. Huxohl, and F. Kummert, "Region Detection Rate: An Applied Measure for Surface Defect Localization," 2021 IEEE International Conference on Signal and Image Processing Applications (ICSIPA), 2021.`

## References
* `O. Silvén, M. Niskanen, and H. Kauppinen, “Wood inspection with non-supervised clustering,” Machine Vision and Applications, vol. 13, no. 5, pp. 275–285, 2003.`
